package com.cursodewicket;

import java.util.Calendar;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

public class Paineis extends Template {

	private static final long serialVersionUID = 1L;

	private String dataAtual = null;

	@Override
	protected void onConfigure() {
		super.onConfigure();
		dataAtual = Calendar.getInstance().getTime().toString();
	}

	public Paineis() {
		add(new BookmarkablePageLink<Lista>("lista", Lista.class));

		dataAtual = Calendar.getInstance().getTime().toString();

		add(new Label("data", dataAtual));
		add(new Label("dataModel", Model.of(dataAtual)));
		add(new Label("dataPropriedade", new PropertyModel<String>(this, "dataAtual")));
		setDefaultModel(new CompoundPropertyModel<Paineis>(this));
		add(new Label("dataAtual"));

		LoadableDetachableModel<String> ldm = new LoadableDetachableModel<String>() {
			@Override
			protected String load() {
				return Calendar.getInstance().getTime().toString();
			}
		};

		add(new Label("dataLDM", ldm));

		add(new Link("self") {
			@Override
			public void onClick() {
				String msg = getString("mensagem1");
				info(msg);
				
				String msg2 = getString("mensagem2");
				warn(msg2);
				
				String msg3 = getString("mensagem3");
				error(msg3);
			}
		});
		
		add(new FeedbackPanel("feedback"));

		PainelSumario painelSumario = new PainelSumario("ps");
		Borda borda = new Borda("br");
		add(borda);
		borda.add(painelSumario);
		adicionaFrag("Fulano", "panel1");
		adicionaFrag("Ciclano", "panel1_2");
		adicionaFrag("Beltrano", "panel1_3");
		add(new Fragment("panel2", "frag2", this));
	}

	private void adicionaFrag(String nome, String id) {
		Fragment frag = new Fragment(id, "frag1", this);
		frag.add(new Label("l", nome));
		add(frag);
	}
}
