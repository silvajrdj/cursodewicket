package com.cursodewicket;

import org.apache.wicket.authroles.authentication.panel.SignInPanel;

public class SigInPanel_pt_BR extends SignInPanel {

	public SigInPanel_pt_BR(String id) {
		super(id);
	}
	
	public SigInPanel_pt_BR(String id, boolean includeRememberMe) {
		super(id, includeRememberMe);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
