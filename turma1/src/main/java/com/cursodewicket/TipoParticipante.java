package com.cursodewicket;

public enum TipoParticipante {

	ALUNO("Aluno"), PROFESSOR("Professor"), COORDENADOR("Coordenador");

	private String nome;

	TipoParticipante(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return nome;
	}
}
