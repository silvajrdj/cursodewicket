package com.cursodewicket.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cursodewicket.Participante;
import com.cursodewicket.UnidadeFederativa;

@Component
public class CadastroServiceImpl extends GenericDataServiceImpl<Participante> {

	@Override
	public void inicializarDB() {
		//setDB(new ArrayList<Participante>());
		popularComDadosTeste();		
	}
		
	private void popularComDadosTeste() {
		 List<Participante> DB = new ArrayList<Participante>();
		DB.add(new Participante("Gilberto"));
		DB.add(new Participante("Alessandro"));
		DB.add(new Participante("Djalma"));
		DB.add(new Participante("Bruno"));
		
		setDB(DB);
	}

	@Override
	public void inicializarClasseGenericaEmTempoDeExecucao() {
		classeGenericaEmTempoDeExecucao = Participante.class;		
	}

//	@Override
//	public void inicializarClasseGenericaEmTempoDeExecucao() {
//		classeGenericaEmTempoDeExecucao = CadastroServiceImpl.class;
//	}

/*	private static final Logger LOGGER = LoggerFactory
			.getLogger(CadastroServiceImpl.class);

	private static List<Participante> DB = new ArrayList<Participante>();
	static {
		DB.add(new Participante("Gilberto"));
		DB.add(new Participante("Alessandro"));
		DB.add(new Participante("Djalma"));
		DB.add(new Participante("Bruno"));
	}

	public void cadastrar(Participante p) {
		LOGGER.info("Cadastrando o participante: " + p.getNome());
		DB.add(p);
	}

	public void remover(Participante p) {
		LOGGER.info("Removendo o participante: " + p.getNome());
		DB.remove(p);
	}

	public List<Participante> listarTodos() {
		LOGGER.info("Listando todos os participantes");
		return new ArrayList<Participante>(DB);
	}

	public List<Participante> listarPaginado(int first, int count) {
		return DB.subList(first, first + count);
		// como se eu fizesse um SQL usando ROWNUM (Oracle) ou LIMIT (MySQL)
	}

	public int obterCount() {
		return DB.size();
		// como se eu fizesse um SQL COUNT()
	}*/

}
