package com.cursodewicket;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.WebPage;

public class HomePage extends Template {
	private static final long serialVersionUID = 1L;

	// TODO Ver como colocar os htmls de login, logout e acesso negado em pt_br
	
    public HomePage(final PageParameters parameters) {
		add(new Label("version", getApplication().getFrameworkSettings().getVersion()));
        		
		add(new BookmarkablePageLink<Lista>("lista", Lista.class));
		add(new BookmarkablePageLink<Cadastro>("cadastro", Cadastro.class));
		add(new BookmarkablePageLink<ListaUnidadeFederativa>("unidades_federativas", ListaUnidadeFederativa.class));
		
		CursoSession cs = CursoSession.get();
		// cs.adicionarCarrinho(new Carrinho());
		
    }
}
