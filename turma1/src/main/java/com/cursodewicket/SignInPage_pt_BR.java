package com.cursodewicket;

import org.apache.wicket.authroles.authentication.pages.SignInPage;
import org.apache.wicket.authroles.authentication.panel.SignInPanel;
import org.apache.wicket.request.mapper.parameter.PageParameters;

public class SignInPage_pt_BR extends SignInPage  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * Constructor
	 * 
	 * @param parameters
	 *            The page parameters
	 */
	public SignInPage_pt_BR(final PageParameters parameters)
	{
		super.remove("signInPanel");
		super.add(new SigInPanel_pt_BR("signInPanel"))
;		//add(new SigInPanel_pt_BR("signInPanel"));		
	}

}
