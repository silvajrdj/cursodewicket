package com.cursodewicket;

import java.io.Serializable;

public class Participante implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nome;
	private String email;
	private String mensagem;
	private TipoParticipante tipoParticipante;

	public Participante() {
	}

	public Participante(String nome) {
		this.nome = nome;
	}

	public TipoParticipante getTipoParticipante() {
		return tipoParticipante;
	}

	public void setTipoParticipante(TipoParticipante tipoParticipante) {
		this.tipoParticipante = tipoParticipante;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Participante [nome=" + nome + ", email=" + email
				+ ", tipoParticipante=" + tipoParticipante + "]";
	}

}
