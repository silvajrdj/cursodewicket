package com.cursodewicket.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.cursodewicket.UnidadeFederativa;

@Component
public class UnidadeFederativaServiceImpl extends GenericDataServiceImpl<UnidadeFederativa> {

	@Override
	public void inicializarDB() {
		//setDB(new ArrayList<UnidadeFederativa>());
		popularComDadosTeste();
		//setClasseGenericaEmTempoDeExecucao(UnidadeFederativa.class);
	}
	
	private void popularComDadosTeste() {
		List<UnidadeFederativa> DB = new ArrayList<UnidadeFederativa>();
		DB.add(new UnidadeFederativa("RJ","Rio de Janeiro"));
		DB.add(new UnidadeFederativa("DF","Distrito Federal"));
		DB.add(new UnidadeFederativa("GO","Goiás"));
		DB.add(new UnidadeFederativa("MT","Mato Grosso"));
		DB.add(new UnidadeFederativa("SC","Santa Catarina"));
		
		setDB(DB);
	}

	@Override
	public void inicializarClasseGenericaEmTempoDeExecucao() {
		classeGenericaEmTempoDeExecucao = UnidadeFederativa.class;		
	}


	
}
