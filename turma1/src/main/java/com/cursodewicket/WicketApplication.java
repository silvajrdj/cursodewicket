package com.cursodewicket;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.wicket.Page;
import org.apache.wicket.Session;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authentication.pages.SignInPage;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.Request;
import org.apache.wicket.request.Response;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;

import com.cursodewicket.services.GenericDataService;
import com.cursodewicket.services.UnidadeFederativaServiceImpl;
import com.cursodewicket.util.AccessDeniedPage;
import com.cursodewicket.util.InternalErrorPage;
import com.cursodewicket.util.PageExpiredErrorPage;

public class WicketApplication extends AuthenticatedWebApplication {
	
	@Override
	public Class<HomePage> getHomePage() {
		return HomePage.class;
	}

	@Override
	public void init() {
		super.init();
		// getHomePage() ==> localhost:8080/

		mountPage("/cadastro", Cadastro.class);
		// http://localhost:8080/cadastro
		// mountPage("/lista", Lista.class);
		// http://localhost:8080/lista
		// mountPackage("/p", Cadastro.class);
		// mount de todas as páginas do mesmo pacote que a classe Cadastro, na
		// url /p/NomeDaClasse
		// http://localhost:8080/p/Cadastro
		// http://localhost:8080/p/Lista
		// mount(mapper); ==> uso avançado para customizar melhor o controle de
		// mapeamento de URL a páginas do Wicket

		// Integração com Spring
		SpringComponentInjector springInjector = new SpringComponentInjector(
				this);
		getComponentInstantiationListeners().add(springInjector);
		
		// definindo páginas que herdam das páginas default do wicket, e apresentam 
		// a mensagem traduzida para o português
		getApplicationSettings().setAccessDeniedPage(AccessDeniedPage.class);
		getApplicationSettings().setPageExpiredErrorPage(PageExpiredErrorPage.class);
		getApplicationSettings().setInternalErrorPage(InternalErrorPage.class);
				
		
	}

	@Override
	public Session newSession(Request request, Response response) {
		CursoSession cursoSession = new CursoSession(request);
		cursoSession.setLocale(new Locale("pt", "BR"));
		return cursoSession;
	}

	@Override
	protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
		return CursoSession.class;
	}

	@Override
	protected Class<? extends WebPage> getSignInPageClass() {
		return SignInPage_pt_BR.class;
	}
	
}
