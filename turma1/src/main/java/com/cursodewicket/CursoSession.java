package com.cursodewicket;

import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.protocol.http.WebSession;
import org.apache.wicket.request.Request;

public class CursoSession extends AuthenticatedWebSession {

	private static final long serialVersionUID = 1L;

	private Roles roles;

	public CursoSession(Request request) {
		super(request);
	}

	public static CursoSession get() {
		return (CursoSession) WebSession.get();
	}

	@Override
	public Roles getRoles() {
		return roles;
	}

	@Override
	public boolean authenticate(final String username, final String password) {
		// autenticar usando consulta no banco
		// consulta LDAP
		// Spring Security
		// o que for
		// aqui pode ser chamado um servico do spring (para autenticar)
		boolean isAdmin = "admin".equals(username) && "admin".equals(password);
		boolean isUser = "user".equals(username) && "user".equals(password);
		boolean isAuthenticated = isAdmin || isUser;

		if (isAuthenticated) {
			roles = new Roles();
			roles.add("USER");

			if (isAdmin) {
				roles.add("ADMIN");
			}
		}

		return isAuthenticated;
	}

}
