package com.cursodewicket.services;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cursodewicket.UnidadeFederativa;

public abstract class GenericDataServiceImpl<T> implements GenericDataService<T> {
	
	protected static Class classeGenericaEmTempoDeExecucao;
	
	private static Logger LOGGER = LoggerFactory
			.getLogger(GenericDataServiceImpl.class);

	private List<T> DB;
	
	
	/*	
	public Class getClasseGenericaEmTempoDeExecucao() {
		return classeGenericaEmTempoDeExecucao;
	}

	public void setClasseGenericaEmTempoDeExecucao(
			Class classeGenericaEmTempoDeExecucao) {
		this.classeGenericaEmTempoDeExecucao = classeGenericaEmTempoDeExecucao;
	}
*/
	public GenericDataServiceImpl() {		
		inicializarClasseGenericaEmTempoDeExecucao();
		LOGGER = LoggerFactory
				.getLogger(classeGenericaEmTempoDeExecucao);
		inicializarDB();
	}
	
	public abstract void inicializarClasseGenericaEmTempoDeExecucao();
	public abstract void inicializarDB();
	
	public void setDB(List<T> dB) {
		DB = dB;
	}

	/* (non-Javadoc)
	 * @see com.cursodewicket.services.GenericDataService#cadastrar(com.cursodewicket.UnidadeFederativa)
	 */
	public void cadastrar(T p) {
		LOGGER.info("Cadastrando "  + p.toString());
		DB.add(p);
	}

	/* (non-Javadoc)
	 * @see com.cursodewicket.services.GenericDataService#remover(com.cursodewicket.UnidadeFederativa)
	 */
	public void remover(T p) {
		LOGGER.info("Removendo " + p.toString());
		DB.remove(p);
	}

	/* (non-Javadoc)
	 * @see com.cursodewicket.services.GenericDataService#listarTodos()
	 */
	public List<T> listarTodos() {
		LOGGER.info("Listando " + DB.size() + " " + classeGenericaEmTempoDeExecucao.getName() /*+ " " + DB.getClass().getName() + " " + classeGenericaEmTempoDeExecucao.getName()*/);				
		return new ArrayList<T>(DB) ;
	}

	/* (non-Javadoc)
	 * @see com.cursodewicket.services.GenericDataService#listarPaginado(int, int)
	 */
	public List<T> listarPaginado(int first, int count) {
		LOGGER.info("Listando Paginado " + first + " " + (first + count) + " de " + " " + classeGenericaEmTempoDeExecucao.getName() /* + DB.getClass().getName()*/);
		return DB.subList(first, first + count);
		// como se eu fizesse um SQL usando ROWNUM (Oracle) ou LIMIT (MySQL)
	}

	/* (non-Javadoc)
	 * @see com.cursodewicket.services.GenericDataService#obterCount()
	 */
	public int obterCount() {
		LOGGER.info("Contanto " + DB.size() +  " " + classeGenericaEmTempoDeExecucao.getName() /*" " + DB.getClass().getName()*/);
		return DB.size();
		// como se eu fizesse um SQL COUNT()
	}

}
