package com.cursodewicket;

import java.util.Arrays;

import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.validation.IFormValidator;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.apache.wicket.util.string.Strings;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;
import org.apache.wicket.validation.validator.EmailAddressValidator;

import com.cursodewicket.services.CadastroService;

@AuthorizeInstantiation("ADMIN")
public class Cadastro extends Template {

	private static final long serialVersionUID = 1L;
	private TextField<String> nomeField;
	private TextField<String> emailField;
	private TextArea<String> mensagemField;
	private IModel<Participante> participanteModel;

	@SpringBean
	private CadastroService servico;

	public Cadastro(PageParameters pp) {
		pp.get("nome"); // obter ?nome=foobar

		add(new BookmarkablePageLink<Lista>("lista", Lista.class));
		add(new FeedbackPanel("feedback"));

		participanteModel = new CompoundPropertyModel<Participante>(
				new Participante());

		setDefaultModel(participanteModel); // --> agora sim, a pagina tem um
											// Model associado

		Form<Participante> formCadastro = new Form<Participante>(
				"formCadastro", participanteModel) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSubmit() {
				super.onSubmit();

				Participante p = getModelObject(); // eh a mesma coisa que
													// participanteModel.getObject()

				servico.cadastrar(p);

				// info(getString("cadastro.sucesso"));

				// setResponsePage(Cadastro.class); // redireciona o usuario
				// para uma nova instancia da pagina Cadastro

				// Cadastro c = new Cadastro(new PageParameters());
				// c.info(getString("cadastro.sucesso"));
				// setResponsePage(c);

				// melhor forma de limpar o form
				info(getString("cadastro.sucesso"));
				setModelObject(new Participante()); // eh a mesma coisa que
													// participanteModel.setObject(new
													// Participante());
			}

			@Override
			protected void onValidate() {
				// como interromper a validacao? simples, soh nao continuar com
				// ela
				// if (hasError()) {
				// return;
				// }

				// texto (antes da conversão) do que o usuário digitou
				// exatamente o que veio do request
				String nomeDigitado = nomeField.getInput();

				// texto digitado pelo usuário, após conversão de algum
				// IConverter no componente
				String cnomedigitado = nomeField.getConvertedInput();

				/*
				 * String mensagem = mensagemField.getInput(); if
				 * (!Strings.isEmpty(mensagem)) { String email =
				 * emailField.getInput(); if (!Strings.isEmpty(email)) { //
				 * error("Componente email é obrigatorio"); --> mensagem // crua
				 * // error(getString("Required")); --> contexto do form
				 * emailField.error(emailField.getString("Required")); // mais
				 * // adequada } }
				 */
			}
		};

		// Campo Nome
		formCadastro.add(nomeField = new TextField<String>("nome"));
		nomeField.setRequired(true); // nome é obrigatório

		// Campo Email
		emailField = new TextField<String>("email");
		formCadastro.add(emailField);
		emailField.add(EmailAddressValidator.getInstance());
		emailField.add(new IValidator<String>() {
			private static final long serialVersionUID = 1L;

			public void validate(IValidatable<String> validatable) {
				if (validatable.isValid()) {
					String input = validatable.getValue();
					if (!input.endsWith("@gmail.com")) {
						ValidationError erro = new ValidationError();
						erro.addMessageKey("usuario.gmail");
						validatable.error(erro);
						// error(getString("foo")); --> nao eh correto.
					}
				}
			}
		});

		// Campo Mensagem
		mensagemField = new TextArea<String>("mensagem");
		formCadastro.add(mensagemField);

		// Campo Tipo Participante
		final DropDownChoice<TipoParticipante> tiposParticipantes = new DropDownChoice<TipoParticipante>(
				"tipoParticipante", Arrays.asList(TipoParticipante.values())) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void onSelectionChanged(TipoParticipante newSelection) {
				// atualizar outros campos conforme a seleção do usuario
				WebPage page = (WebPage) getParent().getParent(); // retorna a
																	// pagina
				TextField nomef = (TextField) page
						.get("formCadastro:nome:subcomponents:child:child");
				// tiposParticipantes.setChoices(novaListaDeOpcoes);
			}
		};
		tiposParticipantes.setRequired(false); // default
		tiposParticipantes.setNullValid(true); // deve ser true se o campo for
												// realmente nao obrigatorio

		formCadastro.get("nome"); // ==> retorna o objeto 'nomeField'

		// como customizar a apresentação dos elementos de uma dropdownchoice
		tiposParticipantes
				.setChoiceRenderer(new IChoiceRenderer<TipoParticipante>() {
					private static final long serialVersionUID = 1L;

					public Object getDisplayValue(TipoParticipante object) {
						return object.getNome();
					}

					public String getIdValue(TipoParticipante object, int index) {
						// return Integer.toString(index); ==> default do
						// Framework
						return object.toString();
					}
				});
		formCadastro.add(tiposParticipantes);

		// Validacao condicional
		formCadastro.add(new IFormValidator() {
			private static final long serialVersionUID = 1L;

			public void validate(Form<?> form) {
				if (tiposParticipantes.getConvertedInput() == TipoParticipante.ALUNO) {
					if (Strings.isEmpty(emailField.getInput())) {
						emailField.setRequired(true);
						emailField.validate();
						emailField.setRequired(false);
					}
				}
			}

			/**
			 * Retornar os componentes dependentes para esta validacao
			 */
			public FormComponent<?>[] getDependentFormComponents() {
				return new FormComponent[] { emailField, tiposParticipantes };
			}
		});

		// TextField --> Label
		// TextArea --> Label, mas recomendado: MultiLineLabel (converte /n em
		// <br />)

		add(formCadastro);

		add(new Label("nomeParticipante", new PropertyModel(participanteModel,
				"nome")));
		add(new Label("hashCodeModel"));
		add(new Label("hashCodeObjeto"));
	}

	@Override
	protected void onConfigure() {
		get("hashCodeModel").setDefaultModel(
				Model.of(participanteModel.toString()));
		get("hashCodeObjeto").setDefaultModel(
				Model.of(participanteModel.getObject()));
	}

}
