package com.cursodewicket.services;

import java.util.List;

import com.cursodewicket.Participante;

public interface CadastroService {

	public abstract void cadastrar(Participante p);

	public abstract void remover(Participante p);

	public abstract List<Participante> listarTodos();

	public abstract List<Participante> listarPaginado(int first, int count);

	public abstract int obterCount();

}