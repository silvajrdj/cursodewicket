package com.cursodewicket;

import org.apache.wicket.authroles.authentication.pages.SignOutPage;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;

public class Template extends WebPage {

	private static final long serialVersionUID = 1L;

	public Template() {
		add(new BookmarkablePageLink<SignOutPage>("logoutLink",
				SignOutPage_pt_BR.class));
	}
}
