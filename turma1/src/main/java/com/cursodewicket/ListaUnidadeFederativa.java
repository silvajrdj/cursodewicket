package com.cursodewicket;

import java.util.Iterator;
import java.util.List;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;


import com.cursodewicket.services.GenericDataService;
import com.cursodewicket.services.UnidadeFederativaServiceImpl;

public class ListaUnidadeFederativa extends Template {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private UnidadeFederativaServiceImpl servico;
	/* infelizmente parece q o Spring não consegue verificar o tipo Genérico q foi especificado,
	 * não sabendo distinguir entre todas as classes @Component,
	 * teria que adicionar na anotação @SpringBean o parâmetro nome= 
	 * para definir qual a classe concreta a ser utilizada,
	 * por isso estou utilizando diretamente a classe concreta,
	 * será q tem algum problema para o Wicket eu especificar logo a classe concreta? */
	// private GenericDataService<UnidadeFederativa> servico; 

	private UnidadeFederativaModel modelo;

	public ListaUnidadeFederativa() {

		modelo = new UnidadeFederativaModel();

		add( new ListView<UnidadeFederativa>("listaLV",modelo) {

			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<UnidadeFederativa> item) {
				final UnidadeFederativa p = item.getModelObject();
				item.add(new Label("nome", p.getNome()));
				item.add(new LinkRemover("remover", item.getModelObject()));
			}});

	}


	private class UnidadeFederativaModel extends
	LoadableDetachableModel<List<UnidadeFederativa>> {
		private static final long serialVersionUID = 1L;

		@Override
		protected List<UnidadeFederativa> load() {
			return servico.listarTodos();
		}
	}

	private class UnidadeFederativaDataProvider implements
	IDataProvider<UnidadeFederativa> {

		private static final long serialVersionUID = 1L;

		public void detach() {
		}

		public Iterator<? extends UnidadeFederativa> iterator(int first, int count) {
			return servico.listarPaginado(first, count).iterator();
		}

		public int size() {
			// size deve retornar o total de elementos cadastrados
			return servico.obterCount();
		}

		public IModel<UnidadeFederativa> model(UnidadeFederativa object) {
			return new CompoundPropertyModel<UnidadeFederativa>(object);
		}

	}

	private final class LinkRemover extends Link<Void> {
		private final UnidadeFederativa p;
		private static final long serialVersionUID = 1L;

		private LinkRemover(String id, UnidadeFederativa p) {
			super(id);
			this.p = p;
		}

		@Override
		public void onClick() {
			servico.remover(p);
		}
	}
	
}

	

