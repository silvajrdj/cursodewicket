package com.cursodewicket;

import java.util.Iterator;
import java.util.List;

import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeAction;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeActions;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.list.PageableListView;
import org.apache.wicket.markup.html.navigation.paging.PagingNavigator;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.spring.injection.annot.SpringBean;

import com.cursodewicket.services.CadastroService;
import com.cursodewicket.services.CadastroServiceImpl;
import com.cursodewicket.services.GenericDataService;

@AuthorizeInstantiation("USER")
public class Lista extends Template {

	private static final long serialVersionUID = 1L;

	@SpringBean
	private CadastroServiceImpl servico;
	/* infelizmente parece q o Spring não consegue verificar o tipo Genérico q foi especificado,
	 * não sabendo distinguir entre todas as classes @Component,
	 * teria que adicionar na anotação @SpringBean o parâmetro nome= 
	 * para definir qual a classe concreta a ser utilizada,
	 * por isso estou utilizando diretamente a classe concreta,
	 * será q tem algum problema para o Wicket eu especificar logo a classe concreta? */
	//private GenericDataService<Participante> servico;

	private RepeatingView listaRV;

	private ListView<Participante> listaLV;

	private ListaParticipantesModel listModel;

	private PageableListView<Participante> listaPLV;

	public Lista() {
		add(new LinkCadastrar("cadastrar"));

		add(new FeedbackPanel("feedback"));
		add(new PainelSumario("ps"));

		listModel = new ListaParticipantesModel();

		// RepeatingView
		listaRV = new RepeatingView("listaRV");
		add(listaRV);

		// ListView
		listaLV = new ListView<Participante>("listaLV", listModel) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Participante> item) {
				final Participante p = item.getModelObject();
				item.add(new Label("nome", p.getNome()));
				item.add(new LinkRemover("remover", p));
			}
		};
		add(listaLV);

		// PageableListView
		listaPLV = new PageableListView<Participante>("listaPLV", listModel, 2) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(ListItem<Participante> item) {
				final Participante p = item.getModelObject();
				item.add(new Label("nome", p.getNome()));
				item.add(new LinkRemover("remover", p));
			}
		};
		add(listaPLV);
		add(new PagingNavigator("paginacaoPLV", listaPLV));

		// DataView -- bom para acessar banco de dados, limitando em uma faixa
		// de registros
		DataView<Participante> listaDV = new DataView<Participante>("listaDV",
				new ParticipanteDataProvider(), 2) {
			private static final long serialVersionUID = 1L;

			@Override
			protected void populateItem(final Item<Participante> item) {
				item.add(new Label("nome"));
				item.add(new LinkRemover("remover", item.getModelObject()));
			}
		};
		add(listaDV);
		add(new PagingNavigator("paginacao", listaDV));
	}

	@Override
	protected void onConfigure() {
		super.onConfigure();

		List<Participante> listaParticipantes = listModel.getObject();

		listaRV.removeAll();
		for (Participante p : listaParticipantes) {
			listaRV.add(new Label(listaRV.newChildId(), p.getNome()));
		}

		// como ir para a ultima pagina
		listaPLV.setCurrentPage(listaPLV.getPageCount() - 1);
	}

	@AuthorizeActions(actions = { @AuthorizeAction(action = "RENDER", deny = "USER") })
	private final class LinkRemover extends Link<Void> {
		private final Participante p;
		private static final long serialVersionUID = 1L;

		private LinkRemover(String id, Participante p) {
			super(id);
			this.p = p;
		}

		@Override
		public void onClick() {
			servico.remover(p);
		}
	}

	@AuthorizeActions(actions = { @AuthorizeAction(action = "RENDER", deny = "USER") })
	private static final class LinkCadastrar extends Link<Void> {
		private static final long serialVersionUID = 1L;

		private LinkCadastrar(String id) {
			super(id);
		}

		@Override
		public void onClick() {
			setResponsePage(Cadastro.class);
		}
	}

	private class ListaParticipantesModel extends
			LoadableDetachableModel<List<Participante>> {
		private static final long serialVersionUID = 1L;

		@Override
		protected List<Participante> load() {
			return servico.listarTodos();
		}
	}

	private class ParticipanteDataProvider implements
			IDataProvider<Participante> {

		private static final long serialVersionUID = 1L;

		public void detach() {
		}

		public Iterator<? extends Participante> iterator(int first, int count) {
			return servico.listarPaginado(first, count).iterator();
		}

		public int size() {
			// size deve retornar o total de elementos cadastrados
			return servico.obterCount();
		}

		public IModel<Participante> model(Participante object) {
			return new CompoundPropertyModel<Participante>(object);
		}

	}
}
