package com.cursodewicket.services;

import java.util.List;

public interface GenericDataService<T> {

	public abstract void cadastrar(T p);

	public abstract void remover(T p);

	public abstract List<T> listarTodos();

	public abstract List<T> listarPaginado(int first, int count);

	public abstract int obterCount();

	public abstract void setDB(List<T> dB);
	
}