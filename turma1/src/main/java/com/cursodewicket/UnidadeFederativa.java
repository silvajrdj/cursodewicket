package com.cursodewicket;

import java.io.Serializable;

public class UnidadeFederativa implements Serializable  {

	private String Sigla;
	private String Nome;
	
	public UnidadeFederativa() {
	}

	public UnidadeFederativa(String sigla, String nome) {
		super();
		Sigla = sigla;
		Nome = nome;
	}

	public String getSigla() {
		return Sigla;
	}

	public void setSigla(String sigla) {
		Sigla = sigla;
	}

	public String getNome() {
		return Nome;
	}

	public void setNome(String nome) {
		Nome = nome;
	}

	@Override
	public String toString() {
		return "UnidadeFederativa [Sigla=" + Sigla + ", Nome=" + Nome + "]";
	}
	
	
}
